-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 172.31.2.33
-- Generation Time: Aug 10, 2016 at 02:14 PM
-- Server version: 10.0.17-MariaDB-log
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `SingleSignOn`
--

-- --------------------------------------------------------

--
-- Table structure for table `cookie_validate`
--

CREATE TABLE IF NOT EXISTS `cookie_validate` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `cookie_id` char(65) NOT NULL,
  `ip_address` char(45) NOT NULL,
  `expires` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `cookie_vars`
--

CREATE TABLE IF NOT EXISTS `cookie_vars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `validate_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `session_validate`
--

CREATE TABLE IF NOT EXISTS `session_validate` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `session_id` char(65) NOT NULL,
  `ip_address` char(45) NOT NULL,
  `expires` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=379 ;

-- --------------------------------------------------------

--
-- Table structure for table `session_vars`
--

CREATE TABLE IF NOT EXISTS `session_vars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `validate_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10210 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
