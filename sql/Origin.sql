-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: bs.csuu1liw1n1b.eu-west-1.rds.amazonaws.com
-- Generation Time: Sep 13, 2016 at 03:07 PM
-- Server version: 10.0.24-MariaDB
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `BeaconSoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_Campaign`
--

CREATE TABLE IF NOT EXISTS `acc_Campaign` (
  `campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(255) NOT NULL,
  `description` text NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`campaign_id`),
  KEY `Cleint_idx` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_Category`
--

CREATE TABLE IF NOT EXISTS `acc_Category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `Site_id_idx` (`site_id`),
  KEY `Category_Post_idx` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `acc_Client`
--

CREATE TABLE IF NOT EXISTS `acc_Client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `primary_site` int(11) DEFAULT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`client_id`),
  KEY `customer_id_idx` (`customer_id`),
  KEY `PrimarySite_idx` (`primary_site`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_Link`
--

CREATE TABLE IF NOT EXISTS `acc_Link` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `socialNet_id` int(11) DEFAULT NULL,
  `url` text,
  `short` text,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`link_id`),
  KEY `SocialNetwork_idx` (`socialNet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_Page`
--

CREATE TABLE IF NOT EXISTS `acc_Page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` char(255) NOT NULL,
  `description` text NOT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`page_id`),
  KEY `Campaign_idx` (`campaign_id`),
  KEY `Category_idx` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_Post`
--

CREATE TABLE IF NOT EXISTS `acc_Post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `socialNet_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`post_id`),
  KEY `Campgin_idx` (`campaign_id`),
  KEY `SocialNetwork_idx` (`socialNet_id`),
  KEY `Client_idx` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_PostActivity`
--

CREATE TABLE IF NOT EXISTS `acc_PostActivity` (
  `postActivity_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `created` datetime NULL,
  PRIMARY KEY (`postActivity_id`),
  KEY `Post_idx` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_PostActivity_Visitor`
--

CREATE TABLE IF NOT EXISTS `acc_PostActivity_Visitor` (
  `postActivity_id` int(11) NOT NULL,
  `visitor_id` int(11) NOT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  KEY `PostActivity_idx` (`postActivity_id`),
  KEY `Visitor_idx` (`visitor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acc_PostLink`
--

CREATE TABLE IF NOT EXISTS `acc_PostLink` (
  `postLink_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `link_id` int(11) NOT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`postLink_id`),
  KEY `Post_idx` (`post_id`),
  KEY `Link_idx` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_Site`
--

CREATE TABLE IF NOT EXISTS `acc_Site` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `domain` text,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`site_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `acc_SocialNetwork`
--

CREATE TABLE IF NOT EXISTS `acc_SocialNetwork` (
  `socialNet_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `socialNetwork_id` int(11) DEFAULT NULL,
  `oAuthToken` varchar(45) DEFAULT NULL,
  `oAuthSecure` varchar(45) DEFAULT NULL,
  `oAuthExpire` varchar(45) DEFAULT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`socialNet_id`),
  KEY `SocialNetwork_idx` (`socialNetwork_id`),
  KEY `Client_idx` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_Visit`
--

CREATE TABLE IF NOT EXISTS `acc_Visit` (
  `visit_id` int(11) NOT NULL AUTO_INCREMENT,
  `visitor_id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `raw_id` int(11) NOT NULL,
  `ip_address` char(45) NOT NULL,
  `browser_name` varchar(100) NOT NULL,
  `browser_version` varchar(30) NOT NULL,
  `protocol` char(5) NOT NULL,
  `operating_system` varchar(100) NOT NULL,
  `country` varchar(4) NOT NULL,
  `visit_time` datetime NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`visit_id`),
  KEY `Visitor_idx` (`visitor_id`),
  KEY `Post_idx` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `acc_VisitorFlow`
--

CREATE TABLE IF NOT EXISTS `acc_VisitorFlow` (
  `visitor_flow_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_id` int(11) NOT NULL,
  `site_id` int(11) DEFAULT NULL,
  `current_page` text,
  `action_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `raw` text,
  `acc_VisitorFlowcol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`visitor_flow_id`),
  KEY `VistorFlow_Visitor_idx` (`visitor_id`),
  KEY `VisitorFlow_Site_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_Activity`
--

CREATE TABLE IF NOT EXISTS `sys_Activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `display` char(45) DEFAULT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_Activity`
--

INSERT INTO `sys_Activity` (`activity_id`, `display`) VALUES
(1, 'Post Click');

-- --------------------------------------------------------

--
-- Table structure for table `sys_BeaconCustomer`
--

CREATE TABLE IF NOT EXISTS `sys_BeaconCustomer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` char(200) NOT NULL,
  `last_name` char(200) NOT NULL,
  `company` char(250) NOT NULL,
  `addr_1` text NOT NULL,
  `addr_2` text NOT NULL,
  `addr_3` text NOT NULL,
  `addr_city` text NOT NULL,
  `addr_county` text NOT NULL,
  `addr_country` text NOT NULL,
  `addr_postcode` char(10) NOT NULL,
  `phone` char(30) NOT NULL,
  `website` text NOT NULL,
  `agency` bit(1) NOT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_BeaconCustomer`
--

INSERT INTO `sys_BeaconCustomer` (`customer_id`, `first_name`, `last_name`, `company`, `addr_1`, `addr_2`, `addr_3`, `addr_city`, `addr_county`, `addr_country`, `addr_postcode`, `phone`, `website`, `agency`, `added`, `updated`) VALUES
(1, 'Stewart', 'Boucher', '650hours', '71-75 Shelton Street', 'Covent Garden', '', 'London', '', 'United Kingdom', 'WC2H 9JQ', '0203 239 2 650', 'http://www.650hours.com', b'1', '2016-09-13 14:07:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_Domain`
--

CREATE TABLE IF NOT EXISTS `sys_Domain` (
  `domain_id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` text NOT NULL,
  `customer_id` int(11) NOT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`domain_id`),
  KEY `Cutomer_idx` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_Domain`
--

INSERT INTO `sys_Domain` (`domain_id`, `domain`, `customer_id`, `added`, `updated`) VALUES
(1, '650hours.beaconapp.uk', 1, '2016-09-13 14:07:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_Domain_User`
--

CREATE TABLE IF NOT EXISTS `sys_Domain_User` (
  `domain_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `User_idx` (`user_id`),
  KEY `Domain_idx` (`domain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_SocialNetwork`
--

CREATE TABLE IF NOT EXISTS `sys_SocialNetwork` (
  `socialNet_Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `endpoint` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`socialNet_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_SocialNetwork`
--

INSERT INTO `sys_SocialNetwork` (`socialNet_Id`, `name`, `endpoint`) VALUES
(1, 'twitter', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_TrackingData_raw`
--

CREATE TABLE IF NOT EXISTS `sys_TrackingData_raw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `json` text NOT NULL,
  `session_key` varchar(100) DEFAULT NULL,
  `visit_time` datetime NULL,
  `proccessed_time` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_User`
--

CREATE TABLE IF NOT EXISTS `sys_User` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `encryption` varchar(255) NOT NULL,
  `isAdmin` bit(1) NOT NULL DEFAULT b'0',
  `added` datetime NULL,
  `updated` datetime NULL,
  `isAgency` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_Visitor`
--

CREATE TABLE IF NOT EXISTS `sys_Visitor` (
  `visitor_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) DEFAULT NULL,
  `session_key` varchar(100) DEFAULT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`visitor_id`),
  KEY `Site_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_VisitorClassification`
--

CREATE TABLE IF NOT EXISTS `sys_VisitorClassification` (
  `visitorClass_id` int(11) NOT NULL AUTO_INCREMENT,
  `classification` varchar(45) NOT NULL,
  PRIMARY KEY (`visitorClass_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_VisitorData`
--

CREATE TABLE IF NOT EXISTS `sys_VisitorData` (
  `visitorData_id` int(11) NOT NULL AUTO_INCREMENT,
  `visitor_id` int(11) NOT NULL,
  `name` char(50) NOT NULL,
  `value` text NOT NULL,
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`visitorData_id`),
  KEY `Visitor_idx` (`visitor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_VisitorDetails`
--

CREATE TABLE IF NOT EXISTS `sys_VisitorDetails` (
  `visitorDetails_id` int(11) NOT NULL AUTO_INCREMENT,
  `visitor_id` int(11) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `email` text,
  `dob` date DEFAULT NULL,
  `twitterHandle` varchar(20) DEFAULT NULL,
  `classification_id` int(11) DEFAULT '4',
  `added` datetime NULL,
  `updated` datetime NULL,
  PRIMARY KEY (`visitorDetails_id`),
  KEY `Visitor_idx` (`visitor_id`),
  KEY `Classification_idx` (`classification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `acc_Campaign`
--
ALTER TABLE `acc_Campaign`
  ADD CONSTRAINT `Campaign_Client` FOREIGN KEY (`client_id`) REFERENCES `acc_Client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_Category`
--
ALTER TABLE `acc_Category`
  ADD CONSTRAINT `Category_Post` FOREIGN KEY (`post_id`) REFERENCES `acc_Post` (`post_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Category_Site` FOREIGN KEY (`site_id`) REFERENCES `acc_Site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_Client`
--
ALTER TABLE `acc_Client`
  ADD CONSTRAINT `Client_BeaconCustomer` FOREIGN KEY (`customer_id`) REFERENCES `sys_BeaconCustomer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Client_Site` FOREIGN KEY (`primary_site`) REFERENCES `acc_Site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_Link`
--
ALTER TABLE `acc_Link`
  ADD CONSTRAINT `Link_SocialNetwork` FOREIGN KEY (`socialNet_id`) REFERENCES `sys_SocialNetwork` (`socialNet_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_Page`
--
ALTER TABLE `acc_Page`
  ADD CONSTRAINT `Page_Campaign` FOREIGN KEY (`campaign_id`) REFERENCES `acc_Campaign` (`campaign_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Page_Category` FOREIGN KEY (`category_id`) REFERENCES `acc_Category` (`category_id`);

--
-- Constraints for table `acc_Post`
--
ALTER TABLE `acc_Post`
  ADD CONSTRAINT `Post_Campaign` FOREIGN KEY (`campaign_id`) REFERENCES `acc_Campaign` (`campaign_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Post_Client` FOREIGN KEY (`client_id`) REFERENCES `acc_Client` (`client_id`),
  ADD CONSTRAINT `Post_SocialNetwork` FOREIGN KEY (`socialNet_id`) REFERENCES `acc_SocialNetwork` (`socialNet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_PostActivity`
--
ALTER TABLE `acc_PostActivity`
  ADD CONSTRAINT `PostActivity_Post` FOREIGN KEY (`post_id`) REFERENCES `acc_Post` (`post_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_PostActivity_Visitor`
--
ALTER TABLE `acc_PostActivity_Visitor`
  ADD CONSTRAINT `PostActivityVisitor_PostActivity` FOREIGN KEY (`postActivity_id`) REFERENCES `acc_PostActivity` (`post_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `PostActivityVisitor_Visitor` FOREIGN KEY (`visitor_id`) REFERENCES `sys_Visitor` (`visitor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_PostLink`
--
ALTER TABLE `acc_PostLink`
  ADD CONSTRAINT `PostLink_Link` FOREIGN KEY (`link_id`) REFERENCES `acc_Link` (`link_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `PostLink_Post` FOREIGN KEY (`post_id`) REFERENCES `acc_Post` (`post_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_Site`
--
ALTER TABLE `acc_Site`
  ADD CONSTRAINT `Site_Client` FOREIGN KEY (`client_id`) REFERENCES `acc_Client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_SocialNetwork`
--
ALTER TABLE `acc_SocialNetwork`
  ADD CONSTRAINT `SocialNetwork_Client` FOREIGN KEY (`client_id`) REFERENCES `acc_Client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `SocialNetwork_sSocialNetwork` FOREIGN KEY (`socialNetwork_id`) REFERENCES `sys_SocialNetwork` (`socialNet_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_Visit`
--
ALTER TABLE `acc_Visit`
  ADD CONSTRAINT `Visit_Post` FOREIGN KEY (`post_id`) REFERENCES `acc_Post` (`post_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Visit_Visitor` FOREIGN KEY (`visitor_id`) REFERENCES `sys_Visitor` (`visitor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `acc_VisitorFlow`
--
ALTER TABLE `acc_VisitorFlow`
  ADD CONSTRAINT `VisitorFlow_Site` FOREIGN KEY (`site_id`) REFERENCES `acc_Site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `VistorFlow_Visitor` FOREIGN KEY (`visitor_id`) REFERENCES `sys_Visitor` (`visitor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sys_Domain`
--
ALTER TABLE `sys_Domain`
  ADD CONSTRAINT `Domain_Cutomer` FOREIGN KEY (`customer_id`) REFERENCES `sys_BeaconCustomer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sys_Domain_User`
--
ALTER TABLE `sys_Domain_User`
  ADD CONSTRAINT `DomainUser_Domain` FOREIGN KEY (`domain_id`) REFERENCES `sys_Domain` (`domain_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `DomainUser_User` FOREIGN KEY (`user_id`) REFERENCES `sys_User` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sys_Visitor`
--
ALTER TABLE `sys_Visitor`
  ADD CONSTRAINT `Visitor_Site` FOREIGN KEY (`site_id`) REFERENCES `acc_Site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sys_VisitorData`
--
ALTER TABLE `sys_VisitorData`
  ADD CONSTRAINT `VisitorData_Visitor` FOREIGN KEY (`visitor_id`) REFERENCES `sys_Visitor` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sys_VisitorDetails`
--
ALTER TABLE `sys_VisitorDetails`
  ADD CONSTRAINT `VisitorDetails_Classification` FOREIGN KEY (`classification_id`) REFERENCES `sys_VisitorClassification` (`visitorClass_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `VisitorDetails_Visitor` FOREIGN KEY (`visitor_id`) REFERENCES `sys_Visitor` (`visitor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
