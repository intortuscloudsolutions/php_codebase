<?php
// debugging comments out on a live site
	ini_set("display_errors", true);
	error_reporting(E_ALL & ~E_NOTICE);
	define("AUTOLOADER_DEBUG", false);

// shorthand DIRECTORY_SEPARATOR as _DS_
	define("_DS_", DIRECTORY_SEPARATOR);

// loads the intortus autoloader
	include("intortus/autoload.php");

// save bootstrap path
	define("BOOTSTRAP_PATH", dirname(__FILE__)._DS_);

// define required paths for controllers, models and views
	define("MVC_ENABLED", true);
	define("MVC_CONTROLLER_PATH", BOOTSTRAP_PATH."controllers"._DS_);
	define("MVC_MODEL_PATH", BOOTSTRAP_PATH."models"._DS_);
	define("MVC_VIEW_PATH", BOOTSTRAP_PATH."views"._DS_);
	
// add support for method types this allows different methods base on the method used to talk to the server
// E.G post to index/index would results in a call to \site\controller\index__post
	define("MVC_SUPPORT_HTTP_METHOD_TYPE", true);

// defines to enable theming
	// tells the controllers we want theming enabled
	define("MVC_THEME_ENABLED", true);
	// tells the controllers where to find the theme header file
	define("MVC_THEME_HEADER", BOOTSTRAP_PATH."views"._DS_."theme"._DS_."header");
	// tells the controllers where to find the theme footer file
	define("MVC_THEME_FOOTER", BOOTSTRAP_PATH."views"._DS_."theme"._DS_."footer");

/* DO NOT EDIT BELOW */

// add support for Constant TRUE = true
if(!defined("TRUE")){
	define("TRUE", true);
}

// add support for Constant FALSE = false
if(!defined("FALSE")){
	define("FALSE", false);
}

// enable the intortus MVC system
if(defined("MVC_ENABLED") && MVC_ENABLED){
	\intortus\mvc\Controller::cron_begin();
}
?>