<?php
namespace site\controller;

use \intortus\storage\Session;
use \intortus\storage\Cookie;
/**
 * Pages Contoller Class this is responsible for the clients Module
 *
 * @author	Martin Barker
 * @version 1.0
 * @package site/controller/Pages
 * @copyrite 2016 Beaconsoft Ltd
 */
 
class Index extends \intortus\mvc\Controller
{
	public function __construct(){
	}
	
	/**
	 * Calls View All (Error Catching)
	 * @access public
	 * @param int $id
	 */ 
	public function index()
	{	
		$view = new \intortus\mvc\View("index/index");
		$view->render();
	}
}