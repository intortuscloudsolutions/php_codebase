<?php
namespace intortus\jsonrpc;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ throw new Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); }

/**
 * @author Martin Barker
 * @date 2015-10-29
 * @description base object for Error and Response handles converting the object into an JSON RPC string
 */
abstract class ReturnableObject{
	public abstract function getResponseString();
	public abstract function getResponseArray();
	public abstract function getResponseObject();
	
	public function __toString(){
		return $this->getResponseString();
	} 
}