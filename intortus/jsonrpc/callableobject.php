<?php
namespace intortus\jsonrpc;

/**
 * @author Martin Barker
 * @description this class provides a type for use on server callable code for security
 */
abstract class CallableObject{
	private static $instance = array();
	protected $connection;
	
	public function setConnection(\intortus\mysql\Connection $connection)
	{
		$this->connection = $connection;
	}
		
	public static function getInstance()
	{
		$cls = get_called_class();
		if(empty(self::$instance[$cls])){
			self::$instance[$cls] = new $cls();
		}
		return self::$instance[$cls];
	}
}
?>