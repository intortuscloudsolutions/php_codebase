<?php
namespace intortus\jsonrpc;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ throw new \Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); }

/**
 * @author Martin Barker
 * @date 2015-10-29
 * @description this is the main server class it handles all management of the server
 */
class Handler{
	private $rawStr;
	private $request; 
	
	private $methods = array();
	private $objects = array();
	
	/**
	 * @description obtains the RPC call from the RAW POST and validates the request
	 */
	public function __construct($str = NULL)
	{
		$this->rawStr = (is_null($str))? \file_get_contents("php://input"):$str;

		$raw = \json_decode($this->rawStr);
		if(!is_object($raw)){
			die($this->InvalidRequest($raw));
		}
		$req = $this->buildRequest($raw);
		if($req instanceof Error){
			echo $req;
			exit;
		}
		$this->request = $req;
	}
	
	/**
	 * @return \intortus\jsonrpc\Request
	 * @description returns the Request as an object
	 */
	public function getRequest()
	{
		return $this->request;
	}
	
	private function buildRequest($raw)
	{
		$isInvalid = false;
		
		$req = new Request();
		foreach($raw as $name => $value){
			if($name == "jsonrpc"){
				$req->version = $raw->$name;
			}else{
				if(\property_exists($req, $name)){
					$req->$name = $raw->$name;
				}else{
					$isInvalid = true;
				}
			}
		}
		
		if($isInvalid){
			return $this->InvalidRequest($req);
		}
		return $req;
	}
	
	/**
	 * @param String $objectName the name this object is to be call as in the JSONRPC method 
	 * @param CallableObject $object an object that can be called by this server
	 * @description allows you to create an object in the server that can have methods called on it
	 */
	public function createObject($objectName, CallableObject $object)
	{
		if(array_key_exists($objectName, $this->objects))
		{
			throw new \Exception("The object '$objectName' already exists");
		}else{
			$this->objects[$objectName] = $object;
		}
	}
	
	/**
	 * @param String $methodName the name this method is to be call as in the JSONRPC method 
	 * @param function $method a anonymous function that can be called by this server
	 * @description allows you to create an object in the server that can have methods called on it
	 */
	public function createMethod($methodName, $method)
	{
		if(array_key_exists($methodName, $this->methods))
		{
			throw new \Exception("The function '$methodName' already exists");
		}else{
			$this->methods[$methodName] = $method;
		}
	}
	
	/**
	 * @description handles calling the requested method and providing a response
	 */
	public function begin()
	{
		try{
			return $this->callHandler($this->request->method, $this->request->params);
			exit;
		}catch(\Exception $e){
			error_log($e->getMessage()." Trace:"+$e->getTraceAsString());
			return $this->InternalError($this->request);
			exit;
		}
	}
		
	private function callHandler($methodName,  $params)
	{
		$dotAt = strpos($methodName, '.');
		$response = null;
		if($dotAt === false)
		{
			if(!array_key_exists($methodName, $this->methods))
			{
				return $this->MethodNotFound($this->request);
			}else{
				$reflection = new \ReflectionFunction($this->methods[$methodName]);
				$paramCount = $reflection->getNumberOfParameters();
				$givenParams = count($params);
				if($paramCount == $givenParams)
				{
					$response = call_user_func_array($this->methods[$methodName], $params);
				}else{
					return $this->InvalidParams($this->request);
				}
			}
		}
		else
		{
			$calledMethodParts = explode(".", $methodName);
			if(count($calledMethodParts) > 2){
				return $this->ServerError($this->request, -32001, "Can't call a method on a child object of an object in the servers container");
			}
			$objectName = $calledMethodParts[0];
			$methodName = $calledMethodParts[1];
			if(!array_key_exists($objectName, $this->objects))
			{
				return $this->MethodNotFound($this->request);
			}
			else
			{
				$reflection = new \ReflectionMethod($this->objects[$objectName],$methodName);
				$paramCount = count($reflection->getParameters());
				$givenParams = count($params);
				if($paramCount == $givenParams)
				{
					$response = call_user_func_array(array($this->objects[$objectName],$methodName), $params);
				}else{
					return $this->InvalidParams($this->request);
				}
			}
		}
		
		
		$responseObj = Response::createResponse($this->request);
		$responseObj->setResult($response);
		
		return $responseObj;
	}
	
	/**
	 * @param \introtus\jsonrpc\Request $request the request that caused the error
	 * @description the request was invalid we need to notify the client
	 */
	public function InvalidRequest($request)
	{
		if(is_object($request)){
			return Error::createError(-32601, "Invalid Request", NULL, $request->id);
		}else{
			return Error::createError(-32601, "Invalid Request", NULL, 0);
		}
	}
	
	/**
	 * @param \introtus\jsonrpc\Request $request the request that caused the error
	 * @description the request was for a method that has not been provided to this server we need to report this to the client
	 */
	public function MethodNotFound($request)
	{
		return Error::createError(-32601, "Method not found", NULL, $request->id);
	}

	/**
	 * @param \introtus\jsonrpc\Request $request the request that caused the error
	 * @description the method exists but the incorrect number of paramerts was suppled we need to report this to the clinet
	 */
	public function InvalidParams($request)
	{
		return Error::createError(-32601, "Invalid Params", NULL, $request->id);
	}
	
	/**
	 * @param \introtus\jsonrpc\Request $request the request that caused the error
	 * @description the request was fine but a server error occured we need to notif the client
	 */
	public function InternalError($request)
	{
		return Error::createError(-32601, "Inertnal Error", NULL, $request->id);
	}
	
	/**
	 * @param \introtus\jsonrpc\Request $request the request that caused the error
	 * @param int $code the error code must be between -32000 and -32099
	 * @param string|null $meaning if supplied provides the meaning of this error
	 * @description a custom error was triggered we need to notify the clint
	 */
	public function ServerError($request, $code, $meaning = NULL)
	{
		if($code <= -32000 && $code >= -32099){
			return Error::createError($code, "Server error", NULL, $request->id);
		}else{
			throw new \Exception("A ServerError <b>code</b> must be within rang of -32000 to -32099");
		}
	}
	
	
}