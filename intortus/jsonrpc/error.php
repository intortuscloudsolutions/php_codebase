<?php
namespace intortus\jsonrpc;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ throw new \Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); }

/**
 * @author Martin Barker
 * @date 2015-10-29
 * @description 
 */
class Error extends ReturnableObject{
	public $version = "2.0";
	public $code;
	public $message;
	public $meaning;
	public $id = NULL;
	
	private function __construct()
	{
		
	}
	
	public static function createError($code, $msg, $meaning = NULL, $id = NULL)
	{
		$inst = new Error();
		$inst->code = $code;
		$inst->message = $msg;
		$inst->meaning = $meaning;
		$inst->id = $id;
		return $inst;
	}
	
	private function getErrorDataArray()
	{
		$err = array(
			"code" => $this->code,
			"message" => $this->message
		);
		
		if(!is_null($this->meaning)){
			$error["meaning"] = $this->meaning;
		}
		
		return $err;
	}
	
	public function getResponseString()
	{
		return json_encode(array(
			"jsonrpc" => $this->version,
			"error" => $this->getErrorDataArray(),
			"id" => $this->id
		));
	}
	
	public function getResponseArray()
	{
		return array(
			"jsonrpc" => $version,
			"error" => $this->getErrorDataArray(),
			"id" => $this->id
		);
	}
	
	public function getResponseObject()
	{
		return ((object)array(
			"jsonrpc" => $version,
			"error" => $this->getErrorDataArray(),
			"id" => $this->id
		));
	}
}