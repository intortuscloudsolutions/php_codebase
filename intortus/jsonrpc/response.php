<?php
namespace intortus\jsonrpc;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ throw new Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); }

/**
 * @author Martin Barker
 * @date 2015-10-29
 * @description 
 */
class Response extends ReturnableObject{
	public $version = "2.0";
	public $result;
	public $id;
	
	private function __construct()
	{
		
	} 
	
	public static function createResponse(Request $request)
	{
		$ret = new response();
		$ret->id = $request->id;
		return $ret;
	}
	
	public function setResult($result)
	{
		$this->result = $result;
	}
	
	public function getResponseString()
	{
		$data = json_encode(array(
			"jsonrpc" => $this->version,
			"result" => $this->result,
			"id" => $this->id
		));
		return $data;
	}
	
	public function getResponseArray()
	{
		return array(
			"jsonrpc" => $this->version,
			"result" => $this->result,
			"id" => $this->id
		);
	}
	
	public function getResponseObject()
	{
		return ((object)array(
			"jsonrpc" => $this->version,
			"result" => $this->result,
			"id" => $this->id
		));
	}
	
	public function __toString(){
		return $this->getResponseString();
	}
}