<?php
namespace intortus\jsonrpc;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ throw new \Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); }

/**
 * @author Martin Barker
 * @date 2015-10-29
 * @description holds the information for a request
 */
class Request{
	public $version = "2.0";
	public $method = "";
	public $params = "";
	public $id = "";
}