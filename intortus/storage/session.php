<?php
namespace intortus\storage;

/**
 * Session Managment Class maintains session vars and verifies integrity using a DB Table for verification
 * 
 * @package intortus/storage/Session
 * @author Martin Barker
 * @copyright 2016 Intortus Cloud Solutions Ltd
 */

class Session extends \intortus\mvc\Model{
	private static $inst;
	private $session_id;
	private $vars = array(); 
	
	public static function GetInstance()
	{
		if(!isset(self::$inst)){
			self::$inst = new Self();
		}
		return self::$inst;
	}
	
	private function __construct(){
		$db = \site\model\Database::GetInstance()->GetConnection();
		session_start();
		$headers = \getallheaders();
		$res = $db->query("SELECT `id` FROM `session_validate` WHERE `expires` < '".time()."'");
		$session = Secure::GenerateSecureString($_SERVER['HTTP_USER_AGENT'], session_id());
		$session = $session['secure'];
		/*while($row = $res->fetch_assoc())
		{
			$db->query("DELETE FROM `session_vars` WHERE `validate_id` = '{$row['id']}'");
			$db->query("DELETE FROM `session_validate` WHERE `id` = '{$row['id']}'");
		}*/
		
		$sql = "SELECT `id`,`session_id` FROM `session_validate` 
				WHERE `session_id` = '{$session}'";
		$res = $db->query($sql);
		$row = $res->fetch_assoc();
		if(isset($row['id'])){
			$this->session_id = $row['id'];
			$expires = time()+(15*60);
			$sql = "UPDATE `session_validate` SET `expires` = '{$expires}' WHERE 'id' = '{$row['id']}'";
			$db->query($sql);
		}else{
			$secure = Secure::GenerateSecureString("Session");
			$ip = $_SERVER['REMOTE_ADDR'];
			$expires = time()+(15*60);
			$sql = "INSERT INTO `session_validate` VALUES(NULL, '{$session}', '{$ip}', '{$expires}')"; 
			session_id($secure['secure']);
			$db->query($sql);
			$this->session_id = $db->insert_id;
		}
		
		$sql = "SELECT v.`value` FROM  `session_vars` AS v LEFT JOIN  `session_validate` AS s ON v.`validate_id` = s.`id` 
				WHERE s.`session_id` = '{$session}'";
		$res = $db->query($sql);
		if($res){
			$row = $res->fetch_assoc();
			$this->vars = json_decode($row['value'], true);
		}
		error_log("Row: ".print_r($row, true));
	}
	
	public function __set($n, $v){
		$this->vars[$n] = $v;
		$db = \site\model\Database::GetInstance()->GetConnection();
		$sql = "DELETE FROM `session_vars` WHERE `validate_id` = '{$this->session_id}'";
		$db->query($sql);
		$sql = "INSERT INTO `session_vars` VALUES (NULL, '{$this->session_id}', '".$db->escape_string(json_encode($this->vars))."')";
		$db->query($sql);
	}
	
	public function set($n, $v){
		$this->__set($n, $v);
	}
	
	public function __get($n){
		return $this->vars[$n];
	}
	
	public function get($n){
		return $this->__get($n);
	}
	
	public function __isset($varName){
		$reps = isset($this->vars[$varName]);
		return $reps;
	}
	
	public function __unset($varName){
		unset($this->vars[$varName]);
		
		$db = \site\model\Database::GetInstance()->GetConnection();
		$sql = "DELETE FROM `session_vars` WHERE `validate_id` = '{$this->session_id}'";
		$db->query($sql);
		$sql = "INSERT INTO `session_vars` VALUES (NULL, '{$this->session_id}', '".$db->escape_string(json_encode($this->vars))."')";
		$db->query($sql);
	}
	
	public function clear(){
		$db = \site\model\Database::GetInstance()->GetConnection();
		$sql = "DELETE FROM `session_vars` WHERE `validate_id` = '{$this->session_id}'";
		$db->query($sql);
		$sql = "INSERT INTO `session_vars` VALUES (NULL, '{$this->session_id}', '{}')";
		$db->query($sql);
	}
	
}