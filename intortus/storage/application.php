<?php
namespace intortus\storage;

/**
 * Application Storage allows a model to save a value to a table 
 * 
 * @package intortus/storage/Application
 * @author Martin Barker
 * @copyright 2016 Intortus Cloud Solutions Ltd
 */

class Application extends \intortus\mvc\Model{
	private $db;
	private static $inst;
	
	private function __construct(){
		$this->db = \site\model\Database::GetInstance();
	}
	
	public static function GetInstance(){
		if(!isset(self::$inst)){
			self::$inst = new self();
		}
		return self::$inst;
	}
	
	public function set($name, $value, $modelPath = NULL){
		if(!is_null($modelPath)){
			$name = str_replace("\\", "/", (rtrim($modelPath, "\\")."\\".$name));
		}
		$value = base64_encode($value);
		$sql = "SELECT `value` FROM `application_storage` WHERE `name` LIKE ?";
		$stmt = $this->db->GetConnection()->prepare($sql);
		if(!$stmt){
	    	error_log("[DB-QUERY-APP-STORAGE]Prepare failed: (" . $this->db->GetConnection()->errno . ") " . $this->db->GetConnection()->error . " on line ".(__LINE__-2));
		}
		$stmt->bind_param("s", $name);
		$stmt->execute();
		$stmt->bind_result($valueResult);
		$stmt->fetch();
		$stmt->close();
		if(!$valueResult){
			$stmt = $this->db->GetConnection()->prepare("INSERT INTO `application_storage` VALUES(NULL, ?, ?)");
			if(!$stmt){
				error_log("[DB-QUERY-APP-STORAGE]Prepare failed: (" . $this->db->GetConnection()->errno . ") " . $this->db->GetConnection()->error . " on line ".(__LINE__-2));
			}
			$stmt->bind_param("ss", $name, $value);
			$stmt->execute();
			$stmt->close();
		}else{
			$stmt = $this->db->GetConnection()->prepare("UPDATE `application_storage` SET `value`= ? WHERE `name`= ?");
			if(!$stmt){
				error_log("[DB-QUERY-APP-STORAGE]Prepare failed: (" . $this->db->GetConnection()->errno . ") " . $this->db->GetConnection()->error . " on line ".(__LINE__-2));
			}
			$stmt->bind_param("ss", $value, $name);
			$stmt->execute();
			$stmt->close();
		}
	}
	
	public function get($name, $modelPath = NULL){
		if(!is_null($modelPath)){
			$name = str_replace("\\", "/", (rtrim($modelPath, "\\")."\\".$name));
		}
		$sql = "SELECT `value` FROM `application_storage` WHERE `name` LIKE ?";
		$stmt = $this->db->GetConnection()->prepare($sql);
		if(!$stmt){
	    	error_log("[DB-QUERY-APP-STORAGE]Prepare failed: (" . $this->db->GetConnection()->errno . ") " . $this->db->GetConnection()->error . " on line ".(__LINE__-2));
		}
		$stmt->bind_param("s", $name);
		$stmt->execute();
		$stmt->bind_result($value);
		$stmt->fetch();
		$stmt->close();
		return base64_decode($value);
	}
}
?>