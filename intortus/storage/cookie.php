<?php
namespace intortus\storage;

use \intortus\storage\Secure as Secure;

/**
 * Cookie Managment Class maintains cookies and verifies integrity using a DB Table for verification
 * 
 * @package intortus/storage/Cookie
 * @author Martin Barker
 * @copyright 2016 Intortus Cloud Solutions Ltd
 */

class Cookie extends \intortus\mvc\Model{
	private static $inst;
	private $cookie_id;
	private $vars = array(); 
	
	public static function GetInstance()
	{
		if(!isset(self::$inst)){
			self::$inst = new Self();
		}
		return self::$inst;
	}
	
	private function __construct(){
		session_start();
		$db = \site\model\Database::GetInstance()->GetConnection();
		if(!isset($_COOKIE['cookieID'])){
			$cookie = Secure::GenerateSecureString($_SERVER['HTTP_USER_AGENT'], session_id());
			$cookie = $cookie['secure'];
		}else{
			$cookie = $_COOKIE['cookieID'];
		}
		setcookie("cookieID", $cookie, time()+5*365.25*24*60*60);
		$headers = getallheaders();
		$res = $db->query("SELECT `id` FROM `cookie_validate` WHERE `expires` < '{time()}'");
		while($row = $res->fetch_assoc())
		{
			$db->query("DELETE FROM `cookie_vars` WHERE `validate_id` = '{$row['id']}'");
			$db->query("DELETE FROM `cookie_validate` WHERE `id` = '{$row['id']}'");
		}
		
		$sql = "SELECT `id`,`cookie_id` FROM `cookie_validate` 
				WHERE `cookie_id` = '{$cookie}'";
		$res = $db->query($sql);
		$row = $res->fetch_assoc();
		if(isset($row['id'])){
			$this->cookie_id = $row['id'];
			$expires = time()+(5*365.25*24*60*60);
			$sql = "UPDATE `cookie_validate` SET `expires` = '{$expires}'";
			$db->query($sql);
		}else{
			$secure = Secure::GenerateSecureString("Cookie");
			
			$ip = $headers["X-Forwarded-For"];
			$expires = time()+(5*365.25*24*60*60);
			$sql = "INSERT INTO `cookie_validate` VALUES(NULL, '{$cookie}', '{$ip}', '{$expires}')"; 
			$db->query($sql);
			$this->cookie_id = $db->insert_id;
		}
		
		$sql = "SELECT `obj` FROM `cookie_vars` WHERE `validate_id`='{$this->cookie_id}'";
		$res = $db->query($sql);
		if($res){
			$row = $res->fetch_assoc();
			$this->vars = json_decode($row['obj'], true);
		}
	}
	
	public function __set($n, $v){
		$this->vars[$n] = $v;
		$db = \site\model\Database::GetInstance()->GetConnection();
		$db->query("DELETE FROM `cookie_vars` WHERE `validate_id` = '{$this->cookie_id}'");
		$db->query("INSERT INTO `cookie_vars` VALUES (NULL, '{$this->cookie_id}', '".$db->escape_string(json_encode($this->vars))."')");
	}
	
	
	public function set($n, $v){
		$this->__set($n, $v);
		return $this;
	}
	
	public function __get($n){
		return $this->vars[$n];
	}
	
	public function __isset($varName){
		return isset($this->vars[$varName]);
	}
	
	public function __unset($varName){
		unset($this->vars[$varName]);
		
		$db = \site\model\Database::GetInstance()->GetConnection();
		$db->query("DELETE FROM `cookie_vars` WHERE `validate_id` = '{$this->cookie_id}'");
		$db->query("INSERT INTO `cookie_vars` VALUES (NULL, '{$this->cookie_id}', '".$db->escape_string(json_encode($this->vars))."')");
	}
	
	public function clear(){
		$db = \site\model\Database::GetInstance()->GetConnection();
		$db->query("DELETE FROM `cookie_vars` WHERE `validate_id` = '{$this->cookie_id}'");
		$db->query("INSERT INTO `cookie_vars` VALUES (NULL, '{$this->cookie_id}', '{}')");
	}
}