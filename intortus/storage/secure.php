<?php
namespace intortus\storage;

class Secure extends \intortus\mvc\Model{

	private static function readSalts()
	{
		return array(
			file_get_contents(dirname(__FILE__)."/../../ext/pre.salt"),
			file_get_contents(dirname(__FILE__)."/../../ext/ape.salt")
		);
	}
	
	public static function GenerateSecureString($str, $cypher = NULL){
		$salt = self::readSalts();
		$str = sha1($salt[0].$str.$salt[1]); //holds the sha1 of given string with salts
		// holds the return array
		$ret = array(
			'secure' => NULL, 		 //holds the Secure string for DB comparison
			'cypher' => $cypher		 //holds the cypher used or newly generated one for saving in DB
		);
		if(is_null($cypher)){
			$ret['cypher'] = sha1($salt[0].date("YMDVRSRsmdub74ya47g").microtime().$salt[1]); 
		}
		$secure = "";
		
		for($i = 0; $i < strlen($str); $i++){
			if($i % 3 == 1){
				$secure .= $str[$i].$ret['cypher'][$i];
			}elseif($i % 3 == 2){
				$secure .= $ret['cypher'][$i].$str[$i].$ret['cypher'][$i];
			}
		}
		
		$ret['secure'] = $secure;
		return $ret;
	}

	public static function TripleDES_Encrypt($data, $secret, $iv = NULL)
	{
		$key = md5(utf8_encode($secret), true);
		
		//Take first 8 bytes of $key and append them to the end of $key.
		$key .= substr($key, 0, 8);
		//Padding for 3DES        
		$blockSize = mcrypt_get_block_size('tripledes', 'ecb');
		$len = strlen($data);
		$pad = $blockSize - ($len % $blockSize);
		$data .= str_repeat(chr($pad), $pad);
		
		//Generating iv for 3DES
		if($iv == NULL){
			$iv = chr(0) . chr(0) . chr(0) . chr(0) . chr(0) . chr(0) . chr(0) . chr(0);
		}
		//Encrypt data
		$encData = mcrypt_encrypt(MCRYPT_3DES, $key, $data, MCRYPT_MODE_CBC, $iv);
		
		return base64_encode($encData);
	}
	
	public static function TripleDES_Decrypt($data, $secret, $iv = NULL)
	{
	
		$key = md5(utf8_encode($secret), true);
		
		//Take first 8 bytes of $key and append them to the end of $key.
		$key .= substr($key, 0, 8);
		
		//Generating iv for 3DES
		if($iv == NULL){
			$iv = chr(0) . chr(0) . chr(0) . chr(0) . chr(0) . chr(0) . chr(0) . chr(0);
		}
		//Decrypt data
		$decData = mcrypt_decrypt(MCRYPT_3DES, $key, base64_decode($data), MCRYPT_MODE_CBC, $iv);
		$len = strlen($decData)-1;
		$decData = trim($decData, $decData[$len]);
		return $decData;
	}
}