<?php
namespace \intortus\storage;

class FileHandler{
	// types
	const Read = 'r';
	const ReadWriteStart = 'r+';
	const CleanWrite = 'w';
	const CleanReadWrite = 'w+';
	const ApendOnly = 'a';
	const ApendRead = 'a+';
	const CreateWrite = 'x';
	const CreateReadWrite = 'x+';
	const SafeCreateWrite = 'c';
	const SafeCreateReadWrite = 'c+';
	
	
	private $handle;
	private $path;
	private $method;
	
	public function __construct($path = NULL, $method = FileHandler::ApendRead)
	{
		$this->path = $path;
		$this->method = $method;
	}
	
	public function checkFileExists(){
	}
	
	public function createFile(){
	}
	
	public function deleteFile(){
	}
	
	public function deleteFolder(){
	}
	
	public function writeFile($content){
	}
	
	public function appendFile($content){
	}
	
	public function deleteFolder(){
	}
}