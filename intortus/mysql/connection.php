<?php
namespace intortus\mysql;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ 
	throw new \Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); 
}

class Connection{
	private $connection = null;
	private $cache = null;
	
	public function __construct(
		$host = "",
		$username = "",
		$passwd = "",
		$dbname = "",
		$port = "",
		$socket = ""
	)
	{
		$host = (empty($host))? ini_get("mysqli.default_host"):$host;
		$username = (empty($username))? ini_get("mysqli.default_user"):$username;
		$passwd = (empty($passwd))? ini_get("mysqli.default_pw"):$passwd;
		$port = (empty($port))? ini_get("mysqli.default_port"):$port;
		$socket = (empty($socket))? ini_get("mysqli.default_socket"):$socket;
		
		$this->connection = new \mysqli(
			$host,
			$username, 
			$passwd, 
			$dbname, 
			$port, 
			$socket
		);
	
		if ($this->connection->connect_errno) {
			error_log("Connect failed: %s\n", $this->connection->connect_error);
			$trace = debug_backtrace();
			error_log(print_r($trace, true));
		}

			
		$this->cache = new Cache($this->connection);
	}
	
	public function cache()
	{
		return $this->cache;
	}
	
	public function __get($var)
	{
		if(property_exists($this->connection, $var))
		{
			return $this->connection->$var;
		}
	}
	
	public function __set($var, $val)
	{
		if(property_exists($this->connection, $var))
		{
			$this->connection->$var = $val;
		}
	}
	
	public function __call($method, $args)
	{
		if(method_exists($this->connection, $method)){
			return call_user_func_array(array($this->connection, $method), $args);
		}else{
			trigger_error('Call to undefined method '.__CLASS__.'::'.$method.'()', E_USER_ERROR);
		}
	}
}