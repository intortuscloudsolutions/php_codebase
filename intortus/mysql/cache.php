<?php
namespace intortus\mysql;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ throw new \Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); }

class Cache{
	private $queries = array();
	private $results = array();
	
	private $connection = null;
	
	public function __construct(\mysqli $connection)
	{
		$this->connection = $connection;
	}
	
	public function query($query, $resultmode = MYSQLI_STORE_RESULT)
	{
		$pos = array_search($query, $this->queries);
		if($pos === false)
		{
			$pos = count($this->queries);
			$this->queryies[$pos] = $query;
			$this->results[$pos] = $this->connection->query($query, $resultmode);
		}
		return $this->results[$post];
	}
	
	public function __get($var)
	{
		if(property_exists($this->connection, $var))
		{
			return $this->connection->$var;
		}
	}
	
	public function __set($var, $val)
	{
		if(property_exists($this->connection, $var))
		{
			return $this->connection->$var = $val;
		}
	}
	
	public function __call($method, $args)
	{
		if(method_exists($this->connection, $method)){
			return call_user_func_array(array($this->connection, $method), $args);
		}else{
			trigger_error('Call to undefined method '.__CLASS__.'::'.$method.'()', E_USER_ERROR);
		}
	}
	
}