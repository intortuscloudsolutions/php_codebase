<?php
define("INTORTUS_AUTOLOADER_REGISTERED", true);
spl_autoload_register(function($className){
	$classNSes = explode("\\", $className);
	// the class being loaded is not of the intortus namespace
	// this autoloader should not handle it
	if($classNSes[0] !== "intortus"){
		return false;
	}
	if(constant("AUTOLOADER_DEBUG")){
		echo "Autoloader Intortus should load this file! <br />";
	}
	unset($classNSes[0]);
	$incPath = set_include_path(dirname(__file__));
	if($incPath === false){
		throw new Exception("Unable to set the include path for Intortus Autoloader.");
	}
	$pathToCls = implode(DIRECTORY_SEPARATOR, $classNSes);
	require_once(strtolower($pathToCls.".php"));
	set_include_path($incPath);
}, true, false);
?>