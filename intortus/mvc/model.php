<?php
namespace intortus\mvc;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ 
	throw new \Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); 
}

if(!defined("MVC_MODEL_PATH")){
	throw new \Exception("Intortus Libaray MVC requires 'MVC_MODEL_PATH' to be set");
}

// create the autoloader for models
spl_autoload_register(function($className){
	$classNSes = explode("\\", $className);
	// the class being loaded is not of the intortus namespace
	// this autoloader should not handle it
	if($classNSes[0] === "sites" || $classNSes[1] === "models"){
		error_log("include of class $className failed did you mean to use \\site\\model");
	}
	if($classNSes[0] !== "site" || $classNSes[1] !== "model"){
		return false;
	}
	if(constant("AUTOLOADER_DEBUG")){
		echo "Autoloader Model should load the file for $className! <br />";
	}
	unset($classNSes[0]);unset($classNSes[1]);
	$pathToCls = implode(DIRECTORY_SEPARATOR, $classNSes);
	$incPath = set_include_path(constant("MVC_MODEL_PATH"));
	if(constant("AUTOLOADER_DEBUG")){
		echo "IncludePath: ".constant("MVC_MODEL_PATH")."<br />";
	}
	if($incPath === false){
		throw new Exception("Unable to set the include path for Intortus MVC Model Autoloader.");
	}
	require_once(strtolower($pathToCls.".php"));
	if(constant("AUTOLOADER_DEBUG")){
		echo "file path: ".strtolower($pathToCls.".php")."<br />";
		echo "complete path: ".constant("MVC_MODEL_PATH").strtolower($pathToCls.".php")."<br />";
	}
	set_include_path($incPath);
}, true, false);

class Model{
	private static $insts = array();
	
	public static function begin(){
		
	}
	
	public function set($name, $value){
		\intortus\storage\Application::GetInstance()->set($name, $value, get_called_class());
	}
	
	public function get($name){
		return \intortus\storage\Application::GetInstance()->get($name, get_called_class());
	}
	
	public static function load($model, $newInst = false, $params = NULL){
		$model = "\\site\\model\\".$model;
		$class = new \ReflectionClass($model);
		if($newInst){
			if($class->IsInstantiable()){
				if(is_null($params)){
					return new $model;
				}else{
					return $class->newInstanceArgs($params);
				}
			}else{
				return false;
			}
		}else{
			if(is_null($params)){
				if($class->IsInstantiable() && !isset(self::$insts[$model])){
					if(is_null($params)){
						self::$insts[$model] = new $model;
					}else{
						self::$insts[$model] = $class->newInstanceArgs($params);
					}
				}elseif(isset(self::$insts[$model])){
					return self::$insts[$model];
				}else{
					return false;
				}
			}else{
				if(!is_array($params)){
					$params = array($params);
				}
				if($class->IsInstantiable()){
					self::$insts[$model] = $class->newInstanceArgs($params);
				}else{
					return false;
				}
			}
		}
		
		return self::$insts[$model];
	}

	/**
	 * Check that a post key is set
	 * TODO: validity check on types?
	 *
	 * @access public
	 * @param string $keyname
	 * @return bool true if "valid"
	 */
	public function isValidPost($keyName){
		return isset($_POST[$keyName]) && !empty($_POST[$keyName]);
	}

	/**
	 * Check that a get key is set
	 * TODO: validity check on types?
	 *
	 * @access public
	 * @param string $keyname
	 * @return bool true if "valid"
	 */
	public function isValidGet($keyName){
		return isset($_GET[$keyName]) && !empty($_GET[$keyName]);
	}
}