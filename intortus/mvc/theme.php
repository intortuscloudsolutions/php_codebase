<?php
namespace intortus\mvc;

class Theme{
	protected $header, $footer;
	
	public function __get($name){
		return $this->$name;
	}
	
	public function __construct()
	{
		$this->header = new View(MVC_THEME_HEADER);
		$this->footer = new View(MVC_THEME_FOOTER);
	}
	
	public static function isAjax()
	{
		return (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}
	
	public function render(View $view){
		ob_start();
		if(!self::isAjax())
			$this->header->render();
		$view->render();
		if(!self::isAjax())
			$this->footer->render();
		ob_flush();
	}
	
	public function get(View $view){
		ob_start();
		if(!self::isAjax())
			$this->header->render();
		$view->render();
		if(!self::isAjax())
			$this->footer->render();
		return ob_get_clean();
	}
}