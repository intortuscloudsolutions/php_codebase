<?php
namespace intortus\mvc;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ 
	throw new \Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); 
}

class View{
	// static variables
	private static $viewPath;				// holds the path set to the view path
	
	// instance varaibles
	private $vars; 							// holds all data to be rendered by a view
	private $view;
	
	public $utils;
	
	// static methods
	public static function setViewPath($path){
		self::$viewPath = $path;
	}
	
	public static function isAjax()
	{
		return (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}
	
	public static function debug($obj, $var_dump = false, $pre = true)
	{
		ob_start();
		if($pre){
			echo "<pre>";
		}
		if($var_dump){
			var_dump($obj);
		}else{
			print_r($obj);
		}
		if($pre){
			echo "</pre>";
		}
		return ob_get_clean();
	}
	
	// methods for instances
	public function __construct($view)
	{
		$this->view = $view;
		$this->utils = new \intortus\utils\ViewUtils();
		$this->vars = new \stdClass();
		if(defined("MVC_VIEW_PATH")){
			self::$viewPath = constant("MVC_VIEW_PATH");
		}
		$this->vars->isAjax = self::isAjax();
		
		$uri = explode("/", $_SERVER['REQUEST_URI']);
		
		unset($uri[0], $uri[1], $uri[2]);
		
		$this->_url = array_values($uri);
		
	}
	
	public function __get($name)
	{
		return $this->vars->$name;
	}
	
	public function __set($name, $value)
	{
		return $this->vars->$name = $value;
	}
	
	public function __isset($name)
	{
		return isset($this->vars->$name);
	}
	
	public function __unset($name)
	{
		unset($this->vars->$name);
	}
	
	public function load($view)
	{
		include($view.".phtml");
	}
	
	public function render($path = NULL)
	{
		ob_start();
		$incPath = set_include_path(self::$viewPath);
		if($incPath === false){
			throw new Exception("Unable to set the include path for views.");
		}
		if(is_null($path)){
			include($this->view.".phtml");
		}else{
			include($path.".phtml");
		}
		set_include_path($incPath);
		ob_end_flush();
	}
	
	public function get($path = NULL)
	{
		ob_start();
		$incPath = set_include_path(self::$viewPath);
		if($incPath === false){
			throw new Exception("Unable to set the include path for views.");
		}
		
		if(is_null($path)){
			include($this->view.".phtml");
		}else{
			include($path.".phtml");
		}
		set_include_path($incPath);
		return ob_get_clean();
	}
	
}