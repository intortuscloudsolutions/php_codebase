<?php
namespace intortus\mvc;

if(!defined("INTORTUS_AUTOLOADER_REGISTERED")){ 
	throw new \Exception("Intortus Libaray files can't be loaded outside the intortus autoloader"); 
}

if(!defined("MVC_CONTROLLER_PATH")){
	throw new \Exception("Intortus Libaray MVC requires 'MVC_CONTROLLER_PATH' to be set");
}

// create the autoloader for controllers
spl_autoload_register(function($className){
	$classNSes = explode("\\", $className);
	// the class being loaded is not of the intortus namespace
	// this autoloader should not handle it
	if($classNSes[0] !== "site" || $classNSes[1] !== "controller"){
		return false;
	}
	if(constant("AUTOLOADER_DEBUG")){
		echo "Autoloader Intortus should load this file! '$className'<br />";
	}
	$incPath = set_include_path(constant("MVC_CONTROLLER_PATH"));
	if($incPath === false){
		throw new Exception("Unable to set the include path for Intortus MVC Controller Autoloader.");
	}
	require_once(strtolower($classNSes[2].".php"));
	set_include_path($incPath);
}, true, false);

class Controller{		
	// holds static veriables
	protected static $theme;
	
	// below is static methods
	public static function setTheme(Theme $theme)
	{
		self::$theme = $theme;
	}
	
	public static function begin()
	{		
		if(rtrim($_SERVER['REQUEST_URI'], "/") !== ""){		
			$query = explode('?',$_SERVER['REQUEST_URI']);
			$url = ltrim($query[0], "/");
			$parse_url = str_replace("/", "", rtrim($query[0], "/"));
			$parts = explode("/", $url);
			
			if(strpos($parts[0], ".") !== false){
				header("HTTP/1.0 404 Not Found");
				echo "<h1>404 Not Found</h1><p>Sorry the file you are looking could not be found</p><hr />";
				return false;
			}
			$controllerName = "site\\controller\\".$parts[0];
			$methodName = @$parts[1];
			if(empty($methodName)){ $methodName = "index"; }
			unset($parts[0]);unset($parts[1]);
			$params = array_values($parts);
		}else{
			$controllerName = "site\\controller\\Index";
			$methodName = "index";
			$params = array();
		}
		if(defined("MVC_THEME_ENABLED") && MVC_THEME_ENABLED){
			self::setTheme(new Theme());
		}
		$controller = new $controllerName();
		
		if(defined("MVC_SUPPORT_HTTP_METHOD_TYPE") && constant("MVC_SUPPORT_HTTP_METHOD_TYPE")){
			if(method_exists($controller, $methodName."__options") && $_SERVER['REQUEST_METHOD'] === 'OPTIONS'){
				$methodName .= "__options";
			}
			if(method_exists($controller, $methodName."__post") && $_SERVER['REQUEST_METHOD'] === 'POST'){
				$methodName .= "__post";
			}
			
			if(method_exists($controller, $methodName."__get") && $_SERVER['REQUEST_METHOD'] === 'GET'){
				$methodName .= "__get";
			}
			
			if(method_exists($controller, $methodName."__put") && $_SERVER['REQUEST_METHOD'] === 'PUT'){
				$methodName .= "__put";
			}
			
			if(method_exists($controller, $methodName."__delete") && $_SERVER['REQUEST_METHOD'] === 'DELETE'){
				$methodName .= "__delete";
			}
		}
		call_user_func_array(array($controller, $methodName), $params);
	}
	
	public static function cron_begin() {
		$models = glob(dirname(__FILE__)."/../../models/*.php");
		$run = array();
		
		foreach($models as $model) {
			$filename = explode(_DS_, $model);
			$filenameCount = count($filename);
			$modelName = ucfirst(substr($filename[$filenameCount-1],0,count($filename[$filenameCount-1])-5));
			$modelObj = \intortus\mvc\Model::load($modelName);
			if(is_object($modelObj)){
				if(method_exists($modelObj, "cron")) {
				$order = 0;
					if(isset($modelObj->cronOrder)){
						$order = $modelObj->cronOrder;
					}
					if(!isset($run[$order])){
						$run[$order] = array();
					}
					$run[$order][] = array("name" => $modelName, "path" => $modelObj);
				}
			}
		}
		
		ksort($run);
		unset($models, $model);
		foreach($run as $order => $v) {
			foreach($v as $k=>$model) {
				echo "Started cron for model '{$model[name]}'.\r\n";
				$model["path"]->cron();
				echo "Finished cron for model '{$model[name]}'.\r\n";
			}
		}
	}
	
	// below is instance methods
	
	public function getTheme()
	{
		return self::$theme;
	}
	
	public function __call($method, $args){
		if(method_exists($this, $method)){
			return call_user_func_array(array($this, $method), $args);
		}else{
			trigger_error('Call to undefined method '.__CLASS__.'::'.$method.'()', E_USER_ERROR);
		}
	}
}