<?php
namespace intortus\utils;

/**
 * Email class allows you to use View's as email templates supports text and HTML 
 * 
 * @package intortus/storage/Application
 * @author Martin Barker
 * @copyright 2016 Intortus Cloud Solutions Ltd
 */

use Mailgun\Mailgun;

class Email extends \intortus\mvc\Model{
	private 
		$bTxtFormat,		// are we sending a text version
		$bHtmlFormat,		// are we sending a html version
		$oTxtView,			// holds the text view object
		$oHtmlView,			// holds the html view object
		$aVars = array(),	// holds the template vars for the email views
		$sBoundry;			// holds the boundry for multpart email

	public function __set($name, $value){
		$this->aVars[$name] = $value;
	}

	public function set($name, $value){
		$this->__set($name, $value);
		return $this;
	}
	
	public function __get($name){
		return $this->aVars[$name];
	}
	
	public function get($name){
		return $this->__get($name);
	}
	
	public function setTextTemplate(\intortus\mvc\View $tpl){
		$this->bTxtFormat = true;
		$this->oTxtView = $tpl;
		return $this;
	}
	
	public function setHTMLTemplate(\intortus\mvc\View $tpl){
		$this->bHtmlFormat = true;
		$this->oHtmlView = $tpl;	
		return $this;
	}

	private function buildHeaders($email){
		$this->sBoundry = uniqid('np');
		
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "From: Beaconsoft Console <noreply@beaconapp.uk>\r\n";
		$headers .= "To: ".$email."\r\n";
		if($this->bHtmlFormat && $this->bTxtFormat){
			$headers .= "Content-Type: multipart/alternative;boundary=" . $this->sBoundry . "\r\n";
		}
		return $headers;
	}
	
	private function addHtmlFormat(){
		// pas the vars though
		foreach($this->aVars as $name => $key){
			$this->oHtmlView->$name = $key;
		}
		
		// create the HTML output
		$htmlMsg = $this->oHtmlView->get();
		return $htmlMsg;
	}
	
	private function addTextFormat(){
		// pas the vars though
		foreach($this->aVars as $name => $key){
			$this->oTxtView->$name = $key;
		}
		
		// create the HTML output
		$txtMsg = $this->oTxtView->get();
		return $txtMsg;
	}
	
	public function sendViaMailGun($subject, $emailAddress){
		$mg = Mailgun::create('key-2e19f01f892cf92ff405eaf5bd22fae6');
		// settings
		$settings = [
		  'from'    => 'no-reply@support.thisisbeacon.com', 
		  'to'      => $emailAddress, 
		  'subject' => $subject
		];
		
		if($this->bTxtFormat){
			$settings['text'] = $this->addTextFormat();
		}
		
		if($this->bHtmlFormat){
			$settings['html']	= $this->addHtmlFormat();
		}
		
		// Now, compose and send your message.
		return $mg->messages()->send('support.thisisbeacon.com', $settings);
	}
	
	public function sendEmail($subject, $emailAddress, $viaSMTP = false){
		if($viaSMTP){
			return $this->sendViaSMTP($subject, $emailAddress);
		}else{
			return $this->sendViaMailgun($subject, $emailAddress);
		}
	}
	
	public function sendViaSMTP($subject, $emailAddress){
		$headers = $this->buildHeaders($emailAddress);
		
		if($this->bHtmlFormat && $this->bTxtFormat){
			$message = "This is a MIME encoded message.";
			$message .= "\r\n\r\n--" . $this->sBoundry . "\r\n";
		}
		if($this->bTxtFormat){
			if($this->bHtmlFormat){
				$message .= "Content-type: text/plain;charset=utf-8\r\n";
			}
			$message .= $this->addTextFormat();
			if($this->bHtmlFormat){
				$message .= "\r\n\r\n--" . $this->sBoundry . "\r\n";
			}
		}
		if($this->bHtmlFormat){
			if($this->bTxtFormat){
				$message .= "Content-type: text/html;charset=utf-8\r\n";
			}
			$message .= $this->addHtmlFormat();
			if($this->bTxtFormat){
				$message .= "\r\n\r\n--" . $this->sBoundry . "--\r\n";
			}
		}
		return mail('', $subject, $message, $headers);
	}
}