<?php
namespace site\model;

class Log extends \intortus\mvc\model{
	private static $db;
	
	private static function getDatabase(){
		if(!isset(self::$db)){
			self::$db = \site\model\Database::GetInstance(false);
		}
		return self::$db->getConnection();
	}
	
	public static function openMail($id){
		$sql = "UPDATE `mail` SET `opened` = b'1' , `mail_client` = '{$_SERVER['HTTP_USER_AGENT']}' WHERE `id`='$id'";
		self::getDatabase()->query($sql);
	}
	
	public static function sendMail($user_id, $secure, $subject, $content, $headers){
		$sql = "INSERT INTO `mail` VALUES(
			NULL,
			'$user_id',
			'$subject',
			b'0',
			''
		)";
		self::getDatabase()->query($sql);
		$content .= "<img src='https://clubsandcommunities.com/user/monitor/".self::getDatabase()->insert_id."' />";
		
		\mail("", $subject, $content, $headers);
	}
	
	public static function logSite(){
		// get ip address
		$ip = $_SERVER['REMOTE_ADDR'];
		// get the request time
		$reqTime = $_SERVER['REQUEST_TIME'];
		// get the request method
		$method = $_SERVER['REQUEST_METHOD'];
		// get the query string
		$get = $_SERVER['QUERY_STRING'];
		// get the browsers useragent
		$ua = $_SERVER['HTTP_USER_AGENT'];
		// get the path requested
		$path = $_SERVER['REQUEST_URI'];
		// get the domain used for the request
		$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$domain = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
		// get the raw post
		$post = file_get_contents("php://input");
		
		$sql = "INSERT INTO `site` VALUES(
			NULL, 
			'$ip', 
			'$path',
			'$domain',
			'$reqTime',
			'$method',
			'$get',
			'$post',
			'$ua'
			)";
		self::getDatabase()->query($sql);
	}
	
	public static function logApp(\intortus\jsonrpc\Request $request, $response){
		// get ip address
		$ip = $_SERVER['REMOTE_ADDR'];
		$sql = "INSERT INTO `mobile` VALUES(
			NUll, 
			'$ip', 
			'{$request->method}',  
			'".json_encode($request->params)."',
			'{$request->version}',
			'{$request->id}',
			'{$response}'
		)";
		self::getDatabase()->query($sql);
	}
}