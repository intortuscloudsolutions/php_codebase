<?php
namespace site\model\table;

use site\model\table\search\Advanced as Advanced;
use site\model\table\search\Values as Value;
use site\model\table\search\Column as Column;
use site\model\table\search\Join as Join;

/**
 * Base Table Class this is the parent of all table classes that enables selecting of rows.
 *
 * @author	Martin Barker
 * @version 1.0
 * @package site/model/table/
 * @copyrite 2016 Beaconsoft Ltd
 */
 
class Base{
	// holds the name of the instanced class
	private $className;
	// holds the columns for the table
	protected $columns,
		// holds the values for the current table row
		$values,
		// has the row updated
		$updated,
		// holds the name of the primary key column
		$primary_key,
		// holds wether this is a new entry or not
		$isNew = false,
		// holds the id of this table row
		$id;
	
	// holds accessor to Database Object
	private static $db;
		
	/**
	 * provides access to the Database Object
	 *
	 * @return \site\model\Database $database Database Connection
	 */
	protected static function getDatabase(){
		if(!isset(self::$db)){
			self::$db = \site\model\Database::GetInstance(false);
		}
		return self::$db->getConnection();
	}
	
	/** 
	 * extrapolates the database table name from the name of the called class
	 *
	 * @return String $tableName the table name for the called class
	 */
	public function getTableName(){
		$class = $this->className;
		
		$parts = explode("\\", $class);
		$tableName = $parts[3]."_".$parts[4];
		return $tableName;
	}
	
	/** 
	 * extrapolates the database table name from the name of the given class name
	 *
	 * @param String $class the name of this call obtained from \get_called_class()
	 * @return String $tableName The database table name
	 */
	protected static function getTableFromClassName($class){		
		$parts = explode("\\", $class);
		$tableName = $parts[3]."_".$parts[4];
		return $tableName;
	}
	
	/**
	 * get's the columns and specifiers required for manipuling the table
	 */
	private function getColumns(){
		$db = self::getDatabase();
		$sql = "SHOW COLUMNS FROM `".$this->getTableName()."`";
		$query = $db->query($sql);
		$columns = $values = array();
		while($row = $query->fetch_assoc()){
			$name = $row["Field"];
			$columns[$name] = new \stdClass();
			$columns[$name]->Name = $name;
			$type = strtolower(preg_replace("/\([^)]+\)/","",$row['Type']));
			switch($type){
				case "double":
					$type = "d";
				break;
				case "char":
				case "varchar":
				case "text":
					$type = "s";
				break;
				case "varbinary":
				case "blob":
					$type = "b";
				break;
				default:
					$type = "i";
				break;
			}
			$columns[$name]->Type = $type;
			$columns[$name]->Null = $row['Null'];
			
			$values[$name] = NULL;
			
			if($row['Key'] == "PRI"){
				$this->primary_key = $name;
			}
		}
		$this->values = $values;
		$this->columns = $columns;
	}
	
	 /**
	  * Get the private key column name for the table specified by name
	  * 
	  * @param String $table_name the name of the table we want to get the primary key for
	  * @return String $columsnName Column name for the Primary key column
	  * @throws E_USER_ERROR
	  */
	private static function getPrimaryKeyColumnFromTableName($table_name){
		$db = self::getDatabase();
		$sql = "SHOW COLUMNS FROM `".$table_name."`";
		$columns = $values = array();
		$result = $db->query($sql);
		while($row = $result->fetch_assoc()){
			if($row['Key'] == "PRI"){
				return $row['Field'];
			}
		}
		\trigger_error("Table Dose not have a PRIMARY_KEY column can't be used with Object Manage Data Sources", E_USER_ERROR);
	}
	
	/**
	 * Gives the name of the primary key column
	 * 
	 * @return string $primaryKeyColumnName the primary key database table this class is the accessor for
	 */
	protected function getPrimaryKeyColumn(){
		return $this->primary_key;
	}
	
	/**
	 * Loads the values into this instance of the class
	 * @throws \Exception
	 */
	private function getValues($id){
		$db = self::getDatabase();
		$sql = "SELECT * FROM `".$this->getTableName()."` WHERE `".$this->getPrimaryKeyColumn()."` = ?";
		$stmt = $db->prepare($sql);
		$stmt->bind_param("i", $id);
		
		$bind_results = array();
		foreach($this->columns as $name => $val){
			$bind_results[] = &$this->values[$name];
		}
		call_user_func_array(array($stmt, "bind_result"), $bind_results);
		
		$response = $stmt->execute();
		if(!$response){
			error_log("Database query failed database engine said: '".$stmt->error."'");
			throw new \Exception("Database query failed database engine said: '".$stmt->error."'");
		}
		
		$resultSet = $stmt->get_result();
		while($row = $resultSet->fetch_assoc()){
			foreach($row as $name => $value){
				$this->values[$name] = $value;
			}
		}
	}
	
	/**
	 * when some one asks for a value of the row the called class is an object
	 * it returns the value or triggers an error if column does not exist
	 *
	 * @throws E_USER_NOTICE
	 */
	public function __get($name){
		if(array_key_exists($name, $this->values)){
			return $this->values[$name];
		}
		\trigger_error("Can't get '$name' it is not a column of ".$this->getTableName(), E_USER_NOTICE);
	}
	
	/**
	 * all a value to be retrived without magic methods
	 *
	 * @throws E_USER_NOTICE
	 */
	public function get($name){
		if(array_key_exists($name, $this->values)){
			return $this->values[$name];
		}
		\trigger_error("Can't get '$name' it is not a column of ".$this->getTableName(), E_USER_NOTICE);
	}
	
	/**
	 * when some one sets a value of the row the called class is an object
	 * it will set it or triggers an error if column does not exist
	 *
	 * @throws E_USER_ERROR
	 */
	public function __set($name, $value){
		if(array_key_exists($name, $this->values)){
			if($this->values[$name] != $value){
				$this->updated[$name] = true;
			}
			$this->values[$name] = $value;
		}else{
			
			\trigger_error("Can't set '$name' it is not a column of ".$this->getTableName().\intortus\mvc\View::debug(array($value, $this->values), true, true), E_USER_ERROR);
		}
	}
	
	/**
	 * set the value for the given column without going though magic method
	 * returns the object handler for chaining
	 *
	 * @throws E_USER_ERROR
	 * @return calledClass $this
	 */
	public function set($name, $value){
		$this->__set($name, $value);
		return $this;
	}
	
	/**
	 * returns and instance of child class connected to the current row
	 * 
	 * @param int $id
	 */ 
	public function __construct($id = NULL){
		$this->className = \get_called_class();
		$this->getColumns();
		if(!is_null($id)){
			$this->id = $id;
			$this->getValues($id);
		}else{
			$this->isNew = true;
		}
	}
	
	/**
	 * loads a column and return an instance of this object
	 * @return site/model/table/
	 */ 
	public static function load($id){
		return new $class($params);
	}
	
	/**
	 * allows a user to create a new value for this table
	 * @return site/model/table/
	 */ 
	public static function create(){
		$class = \get_called_class();
		$ist = new $class();
		return $ist;
	}
	
	/**
	 * Updates the values in the Database Table for the row of the called classes table
	 */
	protected function updateValues(){
		if(!empty($this->updated)){
			$db = self::getDatabase();
			$className = \get_called_class();
			$dbTable = self::getTableFromClassName($className);
			$bindParamArgs = array();
			$bindParamArgs[] = "";
			$sql = "UPDATE `".$this->getTableName()."` SET ";
			foreach($this->updated as $name => $val){
				$sql .= "`".$name."` = ?, ";
				$bindParamArgs[0] .= $this->columns[$name]->Type;
				$bindParamArgs[] = &$this->values[$name]; 
			}
			$sql = substr($sql, 0, count($sql)-3)." ";
			$sql .= "WHERE `".self::getPrimaryKeyColumnFromTableName($dbTable)."` = '".$this->id."'";
			
			$stmt = $db->prepare($sql);
			$result = \site\model\Database::getEvaluatedBind($stmt, $bindParamArgs);
			$response = $stmt->execute();
			if(!$response){
				error_log("Insert Values are incorrect Database Server Responded with: '".$stmt->error."'");
				trigger_error("Insert Values are incorrect Database Server Responded with: '".$stmt->error."'", \E_USER_ERROR);
			}
		}
	}
	
	/**
	 * Creates the row in the database if all values are correct
	 */
	 protected function insertValues(){
		if(!empty($this->updated)){
			$db = self::getDatabase();
			$bindParamArgs = array();
			$bindParamArgs[] = "";
			$sql = "INSERT INTO `".$this->getTableName()."` SET ";
			foreach($this->updated as $name => $val){
				$sql .= "`".$name."` = ?, ";
				$bindParamArgs[0] .= $this->columns[$name]->Type;
				$bindParamArgs[] = &$this->values[$name]; 
			}
			$sql = substr($sql, 0, count($sql)-3);
			$stmt = $db->prepare($sql);
			if(!$stmt){
				\trigger_error("MySQLi Query failed MySQL said '".$db->error."'.", \E_USER_ERROR);
			}
			//$result = call_user_func_array(array($stmt, "bind_param"), $bindParamArgs);
			$result = \site\model\Database::getEvaluatedBind($stmt, $bindParamArgs);
			$response = $stmt->execute();
			if(!$response){
				error_log("Insert Values are incorrect Database Server Responded with: '".$stmt->error."'");
				trigger_error("Insert Values are incorrect Database Server Responded with: '".$stmt->error."'", E_USER_ERROR);
			}
			return $stmt->insert_id;
		}
	 }
	 
	 /**
	  * Returns an array of id's for this given search params 
	  * @param array $params must be an array of table\SearchParams
	  */
	  
	/**
	 * Search for id's matching the given array of params
	 *
	 * Returns an array of called class for the matching searchParams
	 * @param Array(search\Value) $searchParams
	 * @return Array(Called Object) 
	 */
	public static function search(\site\model\table\search\Value $searchParams){
		$db = self::getDatabase();
		$className = \get_called_class();
		$dbTable = self::getTableFromClassName($className);
		
		$sql = "SELECT `".self::getPrimaryKeyColumnFromTableName($dbTable)."` as `id` FROM `".$dbTable."` WHERE ";
		$params = array("");
		if(!is_array($searchParams)){
			$searchParams = array($searchParams);
		}
		foreach($searchParams as $sParam){
			$sql .= $sParam->getSearchString($params[0], $params)." AND ";
		}
		$sql = substr($sql, 0, count($sql)-5);
		
		// run the SQL
		$stmt = $db->prepare($sql);
		if(!$stmt){
			echo "Error Creating Statement. '$sql'";
		}
		$result = \site\model\Database::getEvaluatedBind($stmt, $params);
		
		$response = $stmt->execute();
		// fetch the id's
		if(!$stmt){
			\trigger_error("MySQLi Query failed MySQL said '".$stmt->error."'.", \E_USER_ERROR);
		}
		$result = $stmt->get_result();
		$return = array();
		while($row = $result->fetch_assoc()){
			$return[] = new $className($row['id']);
		}
		return $return;
	}
	
	/**
	 * debug Search for id's matching the given array of params
	 *
	 * Returns an SQL String
	 * @param Array(search\Value) $searchParams
	 * @return String
	 */
	
	public static function searchDebug(\site\model\table\search\Value $searchParams){
		$db = self::getDatabase();
		$className = \get_called_class();
		$dbTable = self::getTableFromClassName($className);
		
		$sql = "SELECT `".self::getPrimaryKeyColumnFromTableName($dbTable)."` as `id` FROM `".$dbTable."` WHERE ";
		$params = array("");
		if(!is_array($searchParams)){
			$searchParams = array($searchParams);
		}
		foreach($searchParams as $sParam){
			$sql .= $sParam->getSearchString($params[0], $params)." AND ";
		}
		$sql = substr($sql, 0, count($sql)-5);
		
		$values = $params;
		unset($values[0]);
		$values = array_values($values);
		
		foreach($values as $value){
			$sql = preg_replace("/".preg_quote("?")."/", "'".$db->escape_string($value)."'", $sql, 1);
		}
		return $sql;
	}
	
	/**
	 * Search for id's matching the given array of params
	 *
	 * Returns an array of called class for the matching searchParams
	 * @param Array(search\Value) $searchParams
	 * @return Array(Called Object) 
	 */
	public static function searchCount(\site\model\table\search\Value $searchParams){
		$db = self::getDatabase();
		$className = \get_called_class();
		$dbTable = self::getTableFromClassName($className);
		
		$sql = "SELECT COUNT(`".self::getPrimaryKeyColumnFromTableName($dbTable)."`) as `idCount` FROM `".$dbTable."` WHERE ";
		$params = array("");
		if(!is_array($searchParams)){
			$searchParams = array($searchParams);
		}
		foreach($searchParams as $sParam){
			$sql .= $sParam->getSearchString($params[0], $params)." AND ";
		}
		$sql = substr($sql, 0, count($sql)-5);
		
		// run the SQL
		$stmt = $db->prepare($sql);
		if(!$stmt){
			echo "Error Creating Statement. '$sql'";
		}
		$result = \site\model\Database::getEvaluatedBind($stmt, $params);
		
		$response = $stmt->execute();
		// fetch the id's
		if(!$stmt){
			\trigger_error("MySQLi Query failed MySQL said '".$stmt->error."'.", \E_USER_ERROR);
		}
		$result = $stmt->get_result();
		$row = $result->fetch_assoc();
		return $row['idCount'];
	}
	
	// example Advanced Search (not Impemented)
	/*parent::searchAdvanced(
		new Advanced(
			array(
				new search\Column("ColumnName", "TableName"), 
				new search\Column("id", "TableName2")
			),
			"{$columns[0]->getColumnAs()} LIKE '%?%' AND {tColName,2} = ?"),
			array(
				new SearchParamsTableValues("s", $value),
				new SearchParamsTableValues("i", $id)
			)
		)
	);*/
	public static function searchAdvanced(Advanced $advancedSearch){
		$db = self::getDatabase();
		$className = \get_called_class();
		$dbTable = getTableFromClassName($className);
		$params = array();
		$params[0] = "";
		$sql = $advancedSearch->buildSearch($params[0], $params); 
		// run the SQL
		$stmt = $db->prepare($sql);
		$result = call_user_func_array(array($stmt, "bind_param"), $params);
		$response = $stmt->execute();
		// fetch the id's
		if(!$stmt){
			\trigger_error("MySQLi Query failed MySQL said '".$stmt->error."'.", \E_USER_ERROR);
		}
		$result = $stmt->get_result();
		$return = array();
		while($row = $result->get_result()){
			$return[] = new $className($row['id']);
		}
		return $resturn;
	}
	
	/**
	 * Forces the values to be saved to the Database table if new it will select and reload the class data to populate auto generated values
	 */
	public function forceCommit(){
		if(!$this->isNew){
			$this->updateValues();
			$this->updated = false;
		}else{
			$id = $this->insertValues();
			$this->isNew = false;
			$this->updated = false;
			$this->getValues($id);
		}
	}
	
	/**
	 * Destructor Called on class destory
	 */
	public function __destruct(){
		if(!$this->isNew){
			$this->updateValues();
		}else{
			$this->insertValues();
		}
	}
}