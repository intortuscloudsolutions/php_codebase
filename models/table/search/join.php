<?php
namespace site\model\table\search;

/**
 * SearchParamsColumnValue a search handler class for the basic search of database tables
 *
 * @author	Martin Barker
 * @version 1.0
 * @package site/model/table/search
 * @copyrite 2016 Beaconsoft Ltd
 */
class Join{
	private 
		$tableColumn,
		$tableName,
		$method,
		$search,
		$searchColumn;

	const LEFT = 'LEFT JOIN';
	const RIGHT  = 'RIGHT JOIN';
	const INNER  = 'INNER JOIN';
	const OUTTER  = 'OUTTER JOIN';
	const CROSS  = 'CROSS JOIN';
	
	public function __construct($joiningColumn, $method = Join::INNER)
	{
		if(!is_a($joiningColumn, "\\site\\table\\search\\column")){
			\trigger_error("Join requires you to use the search\\Column class\r\n", \E_USER_ERROR);
		}
		
		if($this->tableName = $joininColumn->getTable() !== false){
			\trigger_error("Join requires the Column to Specify a table\r\n", \E_USER_ERROR);
		}
		$this->tableColumn = $joiningColumn;
		$this->method = $method;
	}
	
	public function addToSearch(Advanced $search,Column $searchColumn){
		$this->search = $search;
		
		if(!is_a($joiningColumn, "\\site\\table\\search\\column")){
			\trigger_error("Join requires you to use the search\\Column class\r\n", \E_USER_ERROR);
		}
				
		$search->addJoin($this);
	}
	
	public function getJoinString(){
		$sql = $this->method." `".$this->tableName."` ON `".
				$this->tableName."`.`".$this->tableColumn->getColumn()."` =";
		if($searchCol = $this->searchColumn->getTable() !== false){
			$sql .= " `".$searchCol."`.";
		}
		$sql .= "`".$this->searchColumn->getColumn()."`";
		return $sql;
		
	}
}