<?php
namespace site\model\table\search;

/**
 * SearchParamsColumnValue a search handler class for the basic search of database tables
 *
 * @author	Martin Barker
 * @version 1.0
 * @package site/model/table/search
 * @copyrite 2016 Beaconsoft Ltd
 */
class Advanced{
	private 
		$joins = array(),
		$columns,
		$syntax,
		$table_as,
		$class;
	public
		$values;
	
	public function __construct($class, $searchColumns, $syntax, $values = NULL, $table_as = NULL){
		if(!is_array($searchColumns)){
			$searchColumns = array($searchColumns);
		}
		foreach($searchColumns as  $key => $col){
			if(!is_a($col, "\\site\\table\\search\\Column")){
				\trigger_error("Columns used in a search must by of type Column", \E_USER_ERROR);
			}
		}
		$this->columns = $searchColumns;
		$this->syntax = $syntax;
		$this->table_as = $table_as;
	}
	
	public function addJoin($join){
		$this->joins[] = $join;
	}
	
	public function buildSearchString(){
		$sql = "SELECT `".$this->class->getPrimaryKeyColumn()."` as `id` FROM `".$this->class->getTableName()."`";
		if(!is_null($this->table_as)){
			$sql .= " AS ".$this->table_as;
		}
		$sql .= " ";
		foreach($this->joins as $join){
			$sql .= trim($join->getJoinString())." "; 
		}
		
		$match = preg_match_all("\{(.*)\}",$this->syntax, $matches);
		$syntax = $this->syntax;
		$columns = $this->columns;
		$column = $this->columns[0];
		foreach($matches as $key => $value){
			$syntax = str_replace("{".$match."}", eval("return ".$match.";"), $syntax);
		}
		if(count($this->values) < substr_count($syntax, "?")){
			\trigger_error("Advanced Search: There are less values provided then paramerters needed", \E_USER_ERROR);
			return false;
		}
		$sql .= " WHERE ".$syntax;
		return $sql;
	}
	
	public function buildSearch(&$dataTypes, &$params){
		$sql = $this->buildSearchString();
		foreach($this->values as $value){
			$dataTypes .= $value->getDataType();
			$params[] = $value->getValue();
		}
		return $sql;
	}
}