<?php
namespace site\model\table\search;

/**
 * SearchParamsColumnValue a search handler class for the basic search of database tables
 *
 * @author	Martin Barker
 * @version 1.0
 * @package site/model/table/search
 * @copyrite 2016 Beaconsoft Ltd
 */
class Column{
	private $table;
	private $column;
	private $column_as;
	
	public function __construct($column, $table = NULL, $cAs = NULL){
		$this->column = $column;
		if(!is_null($table)){
			$this->table = $table;
		}
		if(!is_null($cAs)){
			$this->column_as = $cAs;
		}
	}
	
	public function GetString(){
		return $this->getColumnSelect();
	}
	
	public function getColumnSelect(){
		$sql = "";
		if(isset($this->table)){
			$sql .= "`".$this->table."`.";
		}
		$sql .= "`".$this->column."`";
		if(isset($this->column_as)){
			$sql .= " AS ".$columnAs;
		}
		return $sql;
	}
	
	public function getColumn(){
		if(isset($this->column)){
			return $this->column;
		}else{
			return false;
		}
	}
	
	public function getColumnAs(){
		if(isset($this->column_as)){
			return $this->column_as;
		}else{
			return false;
		}
	}
	
	public function getTable(){
		if(isset($this->table)){
			return $this->table;
		}else{
			return false;
		}
	}
}