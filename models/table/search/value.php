<?php
namespace site\model\table\search;

/**
 * SearchParamsColumnValue a search handler class for the basic search of database tables
 *
 * @author	Martin Barker
 * @version 1.0
 * @package site/model/table/
 * @copyrite 2016 Beaconsoft Ltd
 */
 
 error_log("DB Search Value running");
 
class Value{
 
	// set of constants that lists the supported methods of searching from this class
	const EQUALS = 0;
	const GREATER = 1;
	const GREATEREQUAL = 3;
	const LESS = 4;
	const LESSEQUAL = 5;
	const LIKE = 6;
	const LIKEAPPEND = 7;
	const LIKEPREPEND = 8;
	const LIKEWRAP = 9;
	const IN = 10;

	// holds the column name
	private $columnName;
	// holds the table name
	private $tableName;
	
	// holds the const method from above
	private $method;
	
	// holds teh value type
	private $valueType;
	// holds the value to be used
	private $value;	

	public function __construct($valueType, $value, $columnName = NULL, $method = NULL){
		error_log(2);
		//SearchParamsColumnValue("ColumnName", SearchParamsColumnValue::Equals, "i", $id)
		if(!is_null($columnName)){
			if(!is_object($columnName) && $poss = strpos(".", $columnName) !== false){
				$tableColumn = str_split($columnName, $poss);
				$this->tableName = $tableColumn[0];
				$this->columnName = $tableColumn[1];
			}
			if(is_array($columnName)){
				foreach($columnName as $column){
					if(!is_a($column, "\\site\\model\\table\\search\\Column")){
						\trigger_error("Object not of type \\site\\table\\search\\Column given as objectName", \E_USER_ERROR);
					}
					$this->columnName = $columnName;
				}
			}elseif(is_object($columnName)){
				if(!is_a($columnName, "\\site\\model\\table\\search\\Column")){
					\trigger_error("Object not of type \\site\\table\\search\\Column given as objectName", \E_USER_ERROR);
				}
				$this->columnName = array($columnName);
			}else{
				$this->columnName = array($columnName);
			}
		}
		if(!is_null($method)){
			if( $method < 0 || $method > 10){
				\trigger_error("Given Search method is not valid", E_USER_ERROR);
			}
			$this->method = $method;
		}
		$dataTypes = array("i", "d", "s", "b");
		if( !in_array($valueType, $dataTypes) )
		{
			\trigger_error("Given datatype is not valid for mysqli extension", E_USER_ERROR);
		}
		$this->valueType = $valueType;
		$this->value = $value;
	}
	
	public function ToString(&$dataTypes, &$params){
		return $this->getSearchString($dataTypes, $params);
	}
	
	public function getSearchString(&$dataTypes, &$params){
		$string = "";
		$table = (isset($this->tableName))? "`".$this->tableName."`":"";
		if($this->method < self::IN){
			if(!is_array($this->value)){
				$this->value = array($this->value);
				$this->valueType = array($this->valueType);
			}
			foreach($this->value as $key => $value){
				switch($this->method){
					case self::EQUALS:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." = ?";
						$dataTypes .= $this->valueType[$key];
						$params[] = &$value;
						break;
					}
					case self::GREATER:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." > ?";
						$dataTypes .= $this->valueType[$key];
						$params[] = &$value;
						break;
					}
					case self::GREATEREQUAL:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." >= ?";
						$dataTypes .= $this->valueType[$key];
						$params[] = &$value;
						break;
					}
					case self::LESS:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." < ?";
						$dataTypes .= $this->valueType[$key];
						$params[] = &$value;
						break;
					}
					case self::LESSEQUAL:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." <= ?";
						$dataTypes .= $this->valueType[$key];
						$params[] = &$value;
						break;
					}
					case self::LIKE:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." LIKE ?";
						$dataTypes .= $this->valueType[$key];
						$params[] = &$value;
						break;
					}
					case self::LIKEAPPEND:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." LIKE ?";
						$dataTypes .= $this->valueType[$key];
						$value = $value."%";
						$params[] = &$value;
						break;
					}
					case self::LIKEPREPEND:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." LIKE ?";
						$dataTypes .= $this->valueType[$key];
						$value = "%".$value;
						$params[] = &$value;
						break;
					}
					case self::LIKEWRAP:{
						$string .= " ".$table.$this->columnName[$key]->GetString()." LIKE ?";
						$dataTypes .= $this->valueType[$key];
						$value = "%".$value."%";
						$params[] = &$value;
						break;
					}
				}
				$string .= " AND ";
			}
		}else{
			$string .= " ".$table."`".$this->columnName."` IN (";
			foreach($this->value as $key => $value){
				$string .= "?, ";
				$dataTypes .= $this->valueType[$key];
				$params[] = &$value;
			}
			$string = substr($string, 0, count($string)-2).") AND ";
		}
		return substr($string, 0, count($string)-5);
	}
	
	public function getDataType(){
		return $this->valueType;
	}
	
	public function getValue(){
		return $this->value;
	}
	
}