<?php
namespace site\model\table;

/**
	THIS IS AN EXAMPLE FILE,
	
	this file is an example file for using mysql table though the SQL Manager
	
	this file will look for the table Example in database selected in database.php
	it supports namespaceing tables if you name a table with a prefix e.g prefix_example
	this file would belong at path /models/table/prefix/example.php

 **/

class Example extends \site\model\table\Base{
}