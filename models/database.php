<?php
namespace site\model;

class Database extends \intortus\mvc\model{
	private static $inst;
	private static
		$host = MVC_DATABASE_HOST,
		$user = MVC_DATABASE_DB_NAME,
		$pass = MVC_DATABASE_USERNAME,
		$db = MVC_DATABASE_PASSWORD;
		
	private $connection;
	
	public static function GetInstance($new = false, $h = "", $u="", $p="", $d=""){
		if(!$new){
			if(!isset(self::$inst)){
				self::$inst = new self(new \intortus\mysql\Connection(self::$host, self::$user, self::$pass, self::$db));
			}
			return self::$inst;
		}else{
			return new self(new \intortus\mysql\Connection($h, $u, $p, $d));
		}
	}
	
	private function __construct(\intortus\mysql\Connection $con){
		$this->connection = $con;	
	}
	
	public function GetConnection(){
		return $this->connection;
	}
	
	public static function getEvaluatedBind(\mysqli_stmt &$stmt, &$params, $debug = false){
		$str = '$stmt->bind_param($params[0],';
		$db_out = "";
		if($debug){ 
			$db_out = '$stmt->bind_param(\''.$params[0].'\','; 
		}
		foreach($params as $key => $val){
			if($key == 0){ continue; }
			$str .= '$params['.$key.'],';
			if($debug){ $db_out .= '\''.$val.'\','; }
		}
		$str = substr($str, 0, -1).");";
		if($debug){ 
			substr($db_out, 0, -1).");";
			echo 'eval('.$db_out; 
		} 
		return eval($str);
	}
}
