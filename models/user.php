<?php
namespace site\model;
use \intortus\storage\Session as Session;
use \intortus\storage\Cookie as Cookie;
use \intortus\storage\Secure as Secure;

class UserException extends \Exception{	
	public function __construct($msg, $code){
		$this->message = "UserException: ".$msg; 
		$this->code = $code;
	}
}

class User extends \intortus\mvc\Model{
	// class variables
	private $id, 
			$data, 
			$changes;
			
	// below is static methods	
	private static function sendVerifyEmail($user_id, $first, $email, $secure){
		$emailView = new \intortus\mvc\View("email/verify");
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		
		// Additional headers
		$headers .= "To: $first <$email>" . "\r\n";
		$headers .= 'From: Beaconsoft <no-reply@beaconapp.uk>' . "\r\n";
		
		$emailView->secure = $secure;
		$emailView->email = $email;
		$emailView->name = $first;
		
		// Mail it
		\site\model\Log::sendMail($user_id,  $secure, "Verify Email Address", $emailView->get(), $headers);
	}
	
	// below is class implemented methods
	public static function register($email, $pass, $first, $last){
		$db = Database::GetInstance()->GetConnection();		
		$cypherPass = Secure::GenerateSecureString($pass);
		
		// do a preflight check if user is already registered
		$sql = "SELECT `user_id`, `first_name` 
				FROM `sys_User` 
				WHERE `email` = '".$db->escape_string($email)."'";
		$res = $db->query($sql);
		if($res && $res->num_rows > 0){
			return false;
		}else{
			$verify = Secure::GenerateSecureString($email);	
			$sql = "INSERT INTO `sys_User` VALUES(NULL, 
				'".$db->escape_string($email)."', 
				'".$db->escape_string($first)."', 
				'".$db->escape_string($last)."', 
				'".$db->escape_string($cypherPass['secure'])."', 
				'".$db->escape_string($cypherPass['cypher'])."',
				'".$db->escape_string($verify['cypher'])."',
				b'0',
				NOW(),
				NULL,
				b'1'
			)";
			$result = $db->query($sql);
			
			self::sendVerifyEmail($db->insert_id, $first, $email, $verify['secure']);
			return true;
		}
	}
	
	public static function ResetPasswordRequest($email){
		$db = Database::GetInstance()->GetConnection();		
		$sql = "SELECT `id`, `verify`, `first_name` 
				FROM `sys_User` 
				WHERE `email` = '".$db->escape_string($email)."'";
		$res = $db->query($sql);
		if($res && $res->num_rows > 0){
			$row = $res->fetch_assoc();
			$verify = Secure::GenerateSecureString($email);	
			$sql = "UPDATE `sys_User` 
					SET 
						`bIsForgot` = b'1', 
						`verify` = '".$db->escape_string($verify['cypher'])."'
					WHERE
						`id` = '".$row['id']."'";
			$db->query($sql);
			self::sendPasswordReset($row['first_name'], $email, $verify['secure']);
			return true;
		}else{
			return false;
		}
	}
	
	public static function confirmReset($email, $secure, $password){
		$db = Database::GetInstance()->GetConnection();
		$sql = "SELECT `id`, `verify`, `first_name`, `cypher` FROM `sys_User` WHERE `email` = '".$db->escape_string($email)."'";
		$result = $db->query($sql);
		$row = $result->fetch_assoc();
		$enc = Secure::GenerateSecureString($email, $row['verify']);
		$pass = Secure::GenerateSecureString($password, $row['cypher']);
		if($enc['secure'] == $secure){
			$sql = "UPDATE `sys_User` 
					SET `bIsForgot` =b'0', `verify`='', `password`='{$pass['secure']}' 
					WHERE `id`={$row['id']}";
			$db->query($sql);
			return true;
		}else{
			return false;
		}
	}
	
	public static function verify($verificationCode, $email){
		$db = Database::GetInstance()->GetConnection();
		$sql = "SELECT `id`, `verify`, `first_name` FROM `sys_User` WHERE `email` = '".$db->escape_string($email)."'";
		$result = $db->query($sql);
		$row = $result->fetch_assoc();
		$enc = Secure::GenerateSecureString($email, $row['verify']);
		if($enc['secure'] == $verificationCode){
			$sql = "UPDATE `sys_User` 
					SET `bIsVerified` =b'1', `verify`='' 
					WHERE `id`={$row['id']}";
			$db->query($sql);
			self::sendCompleteEmail($row['first_name'], $email);
			return true;
		}else{
			return false;
		}
	}
	
	public static function sendCompleteEmail($first, $email){
		$emailView = new \intortus\mvc\View("email/complete");
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		
		// Additional headers
		$headers .= "To: $first <$email>" . "\r\n";
		$headers .= 'From: Beaconsoft <no-reply@beaconapp.uk>' . "\r\n";
		
		$emailView->email = $email;
		$emailView->name = $first;
		
		$emailHTML = $emailView->get();
		
		// Mail it
		mail("", "Verify Email Address", $emailHTML, $headers);
	}
	
	public static function sendPasswordReset($first, $email, $link){
		$emailView = new \intortus\mvc\View("email/forgot");
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		
		// Additional headers
		$headers .= "To: $first <$email>" . "\r\n";
		$headers .= 'From: Beaconsoft <no-reply@beaconapp.uk>' . "\r\n";
		
		$emailView->email = $email;
		$emailView->name = $first;
		$emailView->secure = $link;
		
		$emailHTML = $emailView->get();
		
		// Mail it
		mail("", "Password Reset", $emailHTML, $headers);
	}
	
	public static function addCompanyDetails($first, $last, $company, $addr1, $addr2, $addr3, $town, $country, $postcode, $website, $isClient){
		$db = Database::GetInstance()->GetConnection();	
		$sql = "INSERT INTO `sys_BeaconCustomer` VALUES(
			NULL,
			'".$db->escape_string($first)."',
			'".$db->escape_string($last)."',
			'".$db->escape_string($company)."',
			'".$db->escape_string($addr1)."',
			'".$db->escape_string($addr2)."',
			'".$db->escape_string($addr3)."',
			'".$db->escape_string($town)."',
			'',
			'".$db->escape_string($country)."',
			'".$db->escape_string($postcode)."',
			'',
			'".$db->escape_string($website)."',
			b'1',
			NOW(),
			NULL
		)";
		$db->query($sql);
	}
	
	public static function login($email, $pass, $cookie = false, $requireVerified = false){
		$db = Database::GetInstance()->GetConnection();
		$email = $db->escape_string($email);
		$sql = "SELECT `cypher` FROM `sys_User` WHERE `email` = '".$email."'";
		$res = $db->query($sql);
		if($res && $res->num_rows > 0)
		{
			$row = $res->fetch_assoc();
			$password = Secure::GenerateSecureString($pass, $row['cypher']);
			$sql = "SELECT `id` FROM `sys_User` WHERE `password`='".$password['secure']."' AND `email` = '".$email;
			if($requireVerified){ $sql .= "' AND `bIsVerified` = b'1'"; }
			$res = $db->query($sql);
			if($res && $res->num_rows > 0){
				$row = $res->fetch_assoc();
				if($cookie){
					Cookie::GetInstance()->login_id = $row['id']; 
				}
				Session::GetInstance()->login_id = $row['id'];
				return true;
			}
			return -1;
		}
		return -2;
	}
	
	public static function isSessionPresent(){
		return isset(Session::GetInstance()->login_id);
	}
	
	public static function isCookiePresent(){
		return isset(Cookie::GetInstance()->login_id);
	}
}
?>